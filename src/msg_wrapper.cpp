/**
 * @file  msg_wrapper.cpp
 * @brief Serializing roc msg into binary.
 * @author Savva Morozov <savva@mit.edu>
 * @date 6 September 2020
 */

#include "adhoc_comm/msg_wrapper.h"

namespace acl {
namespace adhoc_comm {

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
//      Constructor & Initializer Function
// ----------------------------------------------------------------------------
msgWrapper::msgWrapper(uint8_t id){
    myID_ = id;
}
// ----------------------------------------------------------------------------
msgWrapper::msgWrapper(){
  // empty constructor
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
//      Serializer / deserializer functions
// ----------------------------------------------------------------------------
/* how to serialize a message into a binary
Step 1:
create a buffer of maximum length
Step 2: 
pack values into the buffer using memcpy

first byte - save the type of the message, an integer
second byte - the id of the device; must be a byte (unless you do it differently); this will become the header.frame_id

following bytes: go through each individual value of the message, save it into the buffer, increment length of the buffer
send the buffer together with length
*/
// ----------------------------------------------------------------------------

size_t msgWrapper::packPoseStamped(uint8_t * buf, const geometry_msgs::PoseStamped & msg){
  /*
  Pose Stamped consists of:

  Header header
    uint32 seq
    time stamp
      int32 sec
      int32 nsec
    string frame_id     //  WE DO NOT PASS STRINGS
  Pose pose
    Point position
      float64 x
      float64 y
      float64 z
    Quaternion orientation
      float64 x
      float64 y
      float64 z
      float64 w
  */
  size_t len = 0;

  // set msg ID
  buf[0] = static_cast<uint8_t>( POSE_STAMPED_ID ); len += 1;

  // set my ID
  buf[1] = static_cast<uint8_t>( myID_ ); len += 1;

  // pack values one by one
  memcpy( &buf[len], &msg.header.seq, sizeof(uint32_t) ); len += sizeof(uint32_t);
  memcpy( &buf[len], &msg.header.stamp.sec, sizeof(int32_t) ); len += sizeof(int32_t);
  memcpy( &buf[len], &msg.header.stamp.nsec, sizeof(int32_t) ); len += sizeof(int32_t);

  memcpy( &buf[len], &msg.pose.position.x, sizeof(double) ); len += sizeof(double);
  memcpy( &buf[len], &msg.pose.position.y, sizeof(double) ); len += sizeof(double);
  memcpy( &buf[len], &msg.pose.position.z, sizeof(double) ); len += sizeof(double);

  memcpy( &buf[len], &msg.pose.orientation.x, sizeof(double) ); len += sizeof(double);
  memcpy( &buf[len], &msg.pose.orientation.y, sizeof(double) ); len += sizeof(double);
  memcpy( &buf[len], &msg.pose.orientation.z, sizeof(double) ); len += sizeof(double);
  memcpy( &buf[len], &msg.pose.orientation.w, sizeof(double) ); len += sizeof(double);

  return len;
}

// ----------------------------------------------------------------------------

void msgWrapper::unpackPoseStamped(uint8_t const * buf, size_t len, geometry_msgs::PoseStamped & msg){
  
  /*
  Pose Stamped consists of:

  Header header
    uint32 seq
    time stamp
      int32 sec
      int32 nsec
    string frame_id  // WE DO NOT PASS STRINGS DAMMIT; will insert drone ID here
  Pose pose
    Point position
      float64 x
      float64 y
      float64 z
    Quaternion orientation
      float64 x
      float64 y
      float64 z
      float64 w
  */

  // skip msg ID
  buf += sizeof(uint8_t);
  // handle device ID; requires special handling since we need to get it into a strig. 
  uint8_t id;
  memcpy( &id, buf, sizeof(uint8_t) ); buf += sizeof(uint8_t);
  msg.header.frame_id = std::to_string(id); 

  // pack values into the message one by one
  memcpy( &msg.header.seq, buf, sizeof(uint32_t) ); buf += sizeof(uint32_t);
  memcpy( &msg.header.stamp.sec, buf, sizeof(int32_t) ); buf += sizeof(int32_t);
  memcpy( &msg.header.stamp.nsec, buf, sizeof(int32_t) ); buf += sizeof(int32_t);

  memcpy( &msg.pose.position.x, buf, sizeof(double) ); buf += sizeof(double);
  memcpy( &msg.pose.position.y, buf, sizeof(double) ); buf += sizeof(double);
  memcpy( &msg.pose.position.z, buf, sizeof(double) ); buf += sizeof(double);

  memcpy( &msg.pose.orientation.x, buf, sizeof(double) ); buf += sizeof(double);
  memcpy( &msg.pose.orientation.y, buf, sizeof(double) ); buf += sizeof(double);
  memcpy( &msg.pose.orientation.z, buf, sizeof(double) ); buf += sizeof(double);
  memcpy( &msg.pose.orientation.w, buf, sizeof(double) ); buf += sizeof(double);

}

// ----------------------------------------------------------------------------

uint8_t msgWrapper::getPacketID(uint8_t const * buf, size_t len)
{
  // return the id of the serialized message. 
  // according to above specification, that would be the first byte of the message.
  uint8_t id;
  memcpy( &id, buf, sizeof(uint8_t) );
  return id;
}

// ----------------------------------------------------------------------------

} // ns adhoc_comm
} // ns acl
