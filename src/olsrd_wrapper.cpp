/**
 * @file  olsrd_wrapper.cpp
 * @brief Handling olsrd related tasks.
 * @author Savva Morozov <savva@mit.edu>
 * @date 6 September 2020
 */

#include "adhoc_comm/olsrd_wrapper.h" 

namespace acl {
namespace adhoc_comm {

// ----------------------------------------------------------------------------
//      Constructors
// ----------------------------------------------------------------------------

OLSRD_Wrapper::OLSRD_Wrapper(const std::string& myIP) { 
  myIP_ = myIP;
  net3bytes_ = myIP.substr(0,myIP.find_last_of(".")+1);
}

// ----------------------------------------------------------------------------

OLSRD_Wrapper::OLSRD_Wrapper() { 
  // empty constructor 
}

// ----------------------------------------------------------------------------
//      Functions
// ----------------------------------------------------------------------------


void OLSRD_Wrapper::updOLSRDneighbors(){

  // create a socket
  int olsrd_socket_; 
  olsrd_socket_ = socket(AF_INET, SOCK_STREAM, 0);
  if(olsrd_socket_ < 0) {
    std::cout << "ERROR opening socket\n";
    throw acl::adhoc_comm::SocketException(strerror(errno));
  }
  //specify olsrd address - address of the socket that would listen to olsrd socket
  struct sockaddr_in server_addr;
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons( 2006 );                 // note that this port is the default port for olsrd's txtinfo plugin
  server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

  // connect my socket to the socket that will output olsrd info
  if (connect(olsrd_socket_, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
    std::cout << "ERROR connecting sockets\n";  
    close(olsrd_socket_);
    throw acl::adhoc_comm::SocketException(strerror(errno));
  }

  // write a request
  std::string request = "echo /neigh\r\n"; // request neighbors
  // std::string request = "echo /2hop\r\n"; // request 2-hop neighbors
  // std::string request = "echo /all\r\n";   // request full info
  if (send(olsrd_socket_, request.c_str(), (u_int) request.length(), 0)  <= 0){
    std::cout << "-------------------------------------------\n";
    std::cout << "ERROR sending a request to the olsrd socket\n";
    std::cout << "-------------------------------------------\n";
  }
  
  // receive the info back
  char buffer[OLSRD_LENGTH];
  memset(buffer, 0, OLSRD_LENGTH);
  if (recv(olsrd_socket_, buffer, OLSRD_LENGTH, 0) <= 0){
    std::cout << "--------------------------------------------\n";
    std::cout << "ERROR receiving a request to the olsrd socket\n";
    std::cout << "--------------------------------------------\n";
  }

  // clear the neighbors vector
  neighbors_.clear();

  // find the ip addresses from the buffer
  int i = 0;
  int j = 0;
  while (i < OLSRD_LENGTH){
    if (buffer[i] == net3bytes_.at(j)){
      i++;
      j++;
      if(j == net3bytes_.length()){
        while(isdigit(buffer[i])){
          i++;
          j++;
        }
        char address[j];
        memset(address, 0, j);
        for(int k = 0; k < j; k++)
          address[k] = buffer[i-j+k];
        // std::cout << "$" << address << "$" << "\n";
        // add the address to the vector
        neighbors_.push_back( std::string((char *)&address, j) );
        j = 0;
      }
    }
    else{
      j = 0;
      i++;
    }
  }

  // sending a message that would automatically close the socket
  request = "\r\n";
  if (send(olsrd_socket_, request.c_str(), (u_int) request.length(), 0)  <= 0){
    std::cout << "---------------------------------------------------\n";
    std::cout << "ERROR sending a closing request to the olsrd socket\n";
    std::cout << "---------------------------------------------------\n";
  }
  
}

// ----------------------------------------------------------------------------

void OLSRD_Wrapper::printNeighbors(){
  std::cout << "-------------------------------\n";
  std::cout << "NEIGHBORS:\n";
  for (auto i = neighbors_.begin(); i != neighbors_.end(); ++i)
    std::cout << *i << "\n"; 
  std::cout << "-------------------------------\n";
}


} // ns adhoc_comm
} // ns acl
