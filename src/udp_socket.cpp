/**
 * @file  udp_socket.cpp
 * @brief UDP Socket with Multicast Support. Wraps UNIX socket API.
 * @author Savva Morozov <savva@mit.edu>
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 6 September 2020
 */

#include "adhoc_comm/udp_socket.h"

namespace acl {
namespace adhoc_comm {

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
//      Constructors and initializations
// ----------------------------------------------------------------------------

UDPSocket::UDPSocket(const std::string& localIP, const int commPort)
{
  // initialize the socket using given IP address and port.
  // creates a UDP socket, binds it to the given address, sets buffer sizes, 
  // and allows more multiple processes to connect to the same socket.

  commPort_ = commPort;
  myIP_ = localIP;
  // std::cout << "init on " << commPort_ << "called\n";
  // printFD();

  // create a UDP socket
  socket_ = socket( AF_INET,    // use IPv4 address family
                    SOCK_DGRAM, // use UDP
                    0           // 
                    );
  if (socket_ == -1) throw SocketException(strerror(errno));

  // allow multiple clients on same machine to use address/port
  int y = 1;
  if (setsockopt(socket_, SOL_SOCKET, SO_REUSEADDR, (char *)&y, sizeof(y)) == -1) {
    close(socket_);
    throw SocketException(strerror(errno));
  }

  // create a 1MB buffer
  int optval = BUFSIZE_1MB;
  setsockopt(socket_, SOL_SOCKET, SO_RCVBUF, (char *)&optval, sizeof(optval));
  getsockopt(socket_, SOL_SOCKET, SO_RCVBUF, (char *)&optval, nullptr);
  if (optval != BUFSIZE_1MB) {
    close(socket_);
    throw SocketException(strerror(errno));
  }

  // local address struct
  memset(&localAddr_, 0, sizeof(localAddr_));
  localAddr_.sin_family = AF_INET;
  localAddr_.sin_port = htons(commPort_);
  localAddr_.sin_addr.s_addr = inet_addr(myIP_.c_str()); // htonl(INADDR_ANY)

  // bind the socket to the local address
  if (bind(socket_, (sockaddr *)&localAddr_, sizeof(localAddr_) ) == -1) {
    close(socket_);
    throw SocketException(strerror(errno));
  }
  // std::cout << "init on " << commPort_ << "completed\n";
  // printFD();
}

// ----------------------------------------------------------------------------

UDPSocket::UDPSocket(const int commPort)
  // use INADDR_ANY for localIP
: UDPSocket("0.0.0.0", commPort) {}

// ----------------------------------------------------------------------------

UDPSocket::UDPSocket() {
  //empty socket constructor.
  std::cout << "Empty socket constructor called\n";
  // printFD();
}

// ----------------------------------------------------------------------------

UDPSocket::~UDPSocket()
{
  std::cout << "\nclosing 0\n";
  close(socket_); // be a good computer citizen
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
//      UDP utility functions
// ----------------------------------------------------------------------------

bool UDPSocket::setReceiveTimeout(const int seconds, const int micros)
{
  //set a timeout value of X seconds and Y microseconds.
  // set a receive timeout
  struct timeval timeout = { .tv_sec = seconds, .tv_usec = micros };
  int ret = setsockopt(socket_, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));

  return !(ret == -1);
}

// ----------------------------------------------------------------------------

bool UDPSocket::joinMulticastGroup(const std::string& multicastGroupIP)
{
  // for multicast, the local addr is required to be INADDR_ANY (0.0.0.0).
  // So we will check to see if a specific local IP was given.
  // see https://stackoverflow.com/a/23718680

  if (localAddr_.sin_addr.s_addr != htonl(INADDR_ANY)) {
    throw SocketException("Attempting to join multicast group "
                          "but a localIP was specified.");
  }

  // fill multicast request struct
  struct ip_mreq mreq{};
  mreq.imr_multiaddr.s_addr = inet_addr(multicastGroupIP.c_str());
  mreq.imr_interface = localAddr_.sin_addr;

  // join multicast group
  int ret = setsockopt(socket_, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *) &mreq, sizeof(mreq));

  return !(ret == -1);
}

// ----------------------------------------------------------------------------

bool UDPSocket::receive(char * buf,    // buffer to put the data into 
                        size_t buflen  // length of the buffer
                        )
{
  sockaddr_in remoteAddr{};
  socklen_t addrlen = sizeof(struct sockaddr);

  ssize_t r = recvfrom( socket_,    //receive on this socket fd
                        buf,        //received message will go here
                        buflen,     //specify the size of the buffer, discard the rest
                        0,          
                        (sockaddr *)&remoteAddr, //places the address of the recipient into remoteAddr, casted to sockaddr_in
                        &addrlen);               //length of the address
  return !(r == -1);
}

// ----------------------------------------------------------------------------

bool UDPSocket::sendUDP(   const std::string& remoteIP, 
                        const int remotePort,
                        const char * buf, 
                        const size_t buflen)
{
  // prepape the recipient info   
  sockaddr_in recipient;
  memset(&recipient, 0, sizeof(recipient));
  recipient.sin_family = AF_INET;
  recipient.sin_port = htons(remotePort);
  recipient.sin_addr.s_addr = inet_addr(remoteIP.c_str());
  ssize_t r = sendto(   socket_,                // socket 
                        buf,                    // buffer - that's the message
                        buflen,                 // length of the message
                        0,                      // flag
                        (sockaddr *)&recipient, // cast the recipient into a sockaddr struct
                        sizeof(recipient)       // size of the address, 
                        );
  return !(r == -1);
}


// ----------------------------------------------------------------------------

bool UDPSocket::sendCommUDP(const std::string& remoteIP, const char * buf, const size_t buflen){
  // send a message to the given IP address to the same port as the one we use to send the message.
  // useful for adhoc communication, when all devices talk on the same ports.
  return sendUDP(remoteIP, commPort_, buf, buflen); // and away it goes
}

// ----------------------------------------------------------------------------

void UDPSocket::printFD(){
  // debugging function
  std::cout << "The socket descriptor is\t" << socket_ << "\n";
}

} // ms adhoc_comm
} // ns acl
