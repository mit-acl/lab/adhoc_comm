Ad-hoc Communication Package
===================

## Overview

This ROS package contains C++ wrappers that allow for UDP communication between devices on the same ad-hoc network. 
The purpose of this package is to enable decentralized ROS operation. The router on the networking side and the ros_master on the ROS side create single points of failure (SPoF) in the system. Ad-hoc networks rely on the nodes to determine their routing tables themselves, removing the networking SPoF. We are using the [OLSR](http://www.olsr.org/mediawiki/index.php/Olsr_Daemon) routing protocol to accomolish this (note that we are using version 1 of OLSRd, not 2). On the ROS side, each robot has its own roscore running, and all robot-to-robot communication is handled through the Receptor node - a node that uses UDP sockets to share messages with other devices on the network without using ROS as the middle man. 
You may find more information on this implementation in the ```adhoc_comm_test``` package.

This package provides the following wrappers:
* UDP socket wrapper that handles network sockets and lower level network communication.
* OLSRd wrapper, which uses the olsrd daemon to obtain IP addresses of robot's current neighbors.
* MSG wrapper, which serializes specific ROS msg strctures into binaries that can be sent through the UDP socket.

Among the above, the only files that you as a user would need to edit is the MSG wrapper - by writing serialization and deserialization functions specific to your messages (casually referred to as packing / unpacking functions). Please see ```src/msg_wrapper.cpp``` and ```include/adhoc_comm/msg_wrapper.h``` for the packing/unpacking templates and specific guidelines.

## Usage and Installation

1. Set up an ad-hoc network:
* go to ```/etc/network/interfaces``` and define the ad-hoc network on the interface of your choice. Your settings should look smth like this:
```
allow-hotplug wlan1
iface wlan1 inet static
	wireless-mode ad-hoc
	wireless-channel 4
	wireless-ap 02:12:34:56:78:9A
	wireless-essid adhoc-net
	wireless-key off
	address 10.10.0.10
	netmask 255.255.255.0
	post-up ip route add 10.10.0.0/24 dev wlan1 src 10.10.0.10 table rt2
    post-up ip rule add from 10.10.0.10/32 table rt2
    post-up ip rule add to 10.10.0.10/32 table rt2
```

Also, you might have to comment out the line that sources the ```/etc/network/interfaces.d``` directory to make sure it doesn't mask these settings.

* Edit ```/etc/iproute2/rt_tables``` file by adding the following line in the end: ```1 rt2```. 

* For more information on starting an ad hoc network like that, check out [this blogpost](https://www.thomas-krenn.com/en/wiki/Two_Default_Gateways_on_One_System).

* Restart the network interface or just reboot your computer.


2. Install olsrd:
* ```sudo apt-get install olsrd```
* go to ```/etc/olsrd/``` and replace the default ```olsrd.conf``` with the one included in this package.
* Edit ```olsrd.conf```: In line 21, after ```Interface ``` change the name of the interface to match the interface on which you will be running your ad-hoc network.
* Optional: in ```olsrd.conf``` change the ```DebugLevel``` to 0 if you would like for the daemon to just run in the background; if it is set to 1, the daemon would continiously spit status data to the terminal window that it is run on.
* If using debug level 0, you may use the following command to get olsrd status information: ```echo /all | nc 0.0.0.0 2006```

3. OLSRd can now be run with ```sudo olsrd -f /etc/olsrd/olsrd.conf```.

4. Download from git, unzip, add the package folder to the ```src``` directory of your catkin workspace. Add any message libraries that you are using (```geometry_msgs```, for instance). ```catkin_make``` it.

5. Inside of your ROS package:
* Add ```adhoc_comm``` package to ```find_pacakge()``` of the ```CMakeLists.txt```.
* Add ```adhoc_comm``` as the dependency in ```package.xml```: ```<depend>adhoc_comm</depend>```
* Include the wrappers from the package inside of your ```.h``` or ```.cpp``` files: ```#include "adhoc_comm/udp_socket.h"```




## Checklist for editting the MSG wrapper

### msg_wrapper.h
* ```#include``` your message-specific msg file.
* Define the ```static constexpr``` ID of your message.
* Define message-specific packing and unpacking fucntion prototypes.

### msg_wrapper.cpp
* Write the above packing and unpacking functions following the instructions and the template provided in the file.

Note: If changing the packing format, be sure to edit ```getPacketID()``` to match your packing algorithm.

### CMakeLists.txt
* Add your mesage-specific package to ```find_package()```.

### package.xml
* Add your message-specific package as the dependency.


