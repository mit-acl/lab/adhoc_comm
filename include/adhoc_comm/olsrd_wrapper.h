/**
 * @file  olsrd_wrapper.h
 * @brief Handling olsrd related tasks.
 * @author Savva Morozov <savva@mit.edu>
 * @date 6 September 2020
 */

#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <cerrno>
#include <vector>

#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdexcept>

#include "udp_socket.h" // needed only to throw exceptions

namespace acl {
namespace adhoc_comm {

  class OLSRD_Wrapper
  {
  public:
    static constexpr int OLSRD_LENGTH = 1000; // length of packets received from olsrd socket
    std::vector<std::string> neighbors_; // vector of oslrd neighbors

    /**
     * @brief      Constructor. Determines the IP address for the network over which olsrd is run
     *
     * @param[in]  myIP - ip address of the device; used to determine first 3 bytes of the network; 
     *                    needed for parsin olsrd packets
     * @return     void
     */
    OLSRD_Wrapper(const std::string& myIP);


    /**
     * @brief      Empty constructor; does nothing
     */
    OLSRD_Wrapper();

    /**
     * @brief      Update the local list of neighbors from the OLSRD daemon
     *
     * @param[in]  void
     * 
     * @return     void
     */
    void updOLSRDneighbors();


    /**
     * @brief      Print all neighbors to the screen.
     *
     * @param[in]  void
     * 
     * @return     void
     */
    void printNeighbors();
    

  private:
    std::string myIP_; // IP address used for adhoc comm on this device; unused
    std::string net3bytes_; // first 3 bytes of the adhoc comm network

  };

} // ns adhoc comm
} // ns acl
