/**
 * @file  msg_wrapper.h
 * @brief Serializing roc msg into binary.
 * @author Savva Morozov <savva@mit.edu>
 * @date 6 September 2020
 */

#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <cerrno>

/*
ADD ME: include your additional message libraries
*/ 
#include <geometry_msgs/PoseStamped.h>


namespace acl {
namespace adhoc_comm {
  class msgWrapper
  {
  public:
    /*
    ADD ME: define your additional message IDs.
    */ 
    static constexpr uint8_t POSE_STAMPED_ID = 1; // id of the pose stamped msg
    static constexpr uint8_t OTHER_MSG_ID = 2; // ADD ME: ID of your message


    /**
     * @brief      Contstructor; sets the ID of the device. 
     *             ID of the device can be used to specify the sender of the message.
     *
     * @param[in]  id    The id 
     */
    msgWrapper(uint8_t id);

    /**
     * @brief      Empty contstructor; does nothing.
     */
    msgWrapper();


    /**
     * @brief      Returns the ID of a serialized packet.
     *             Used when a packet is received on a udp socket and a specific 
     *             unpacker fucntion needs to be called.
     *
     * @param[in]  buf    Serialized message 
     * @param[in]  len    Length of the serialized message 
     * 
     * @return     the ID of a given packet
     */
    uint8_t getPacketID(uint8_t const * buf, size_t len);

    /**
     * @brief      Packing function template.
     *
     * @param[in]  buf    Pointer to a buffer to which the message is serialized
     * @param[in]  msg    ROS message
     * 
     * @return     Length of the buffer after serialization. 
     */
    size_t packPoseStamped(uint8_t * buf, const geometry_msgs::PoseStamped & msg);

    /**
     * @brief      Unpacking function template.
     *
     * @param[in]  buf    Pointer to a buffer that needs to be de-serialized
     * @param[in]  len    Length of the buffer
     * @param[in]  msg    ROS message that will be filled with data from buf
     * 
     * @return     void
     */
    void unpackPoseStamped(uint8_t const * buf, size_t len, geometry_msgs::PoseStamped & msg);

    /*
    ADD ME: define your additional packing and upnacking functions
    */ 

  private:
    uint8_t myID_ = 0; // ID of the device sending the data.
  };

} // ns adhoc_comm
} // ns acl
